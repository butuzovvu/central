package com.central.dao;

import com.central.model.Insurance;
import org.apache.ibatis.annotations.*;

import java.util.Collection;

/**
 * Dao страховых организаций
 */
@Mapper
public interface InsuranceDao {

    /**
     * Сохранение страховой организаций
     * @param insurance страховая организация
     */
    @Insert("INSERT INTO central.insurance (name, region, city, address, house, inn, ogrn)" +
            "VALUES(#{name}, #{region}, #{city}, #{address}, #{house}, #{inn}, #{ogrn})")
    @Options(useGeneratedKeys = true, keyColumn = "id")
    void insert(Insurance insurance);

    /**
     * Получение организации
     * @param id идентификатор
     * @return организация
     */
    @Select("SELECT i.* \n" +
            "FROM central.insurance i \n" +
            "WHERE i.id = #{id}")
    Insurance getById(@Param("id") Long id);

    @Select("<script>" +
            "   SELECT i.*\n" +
            "   FROM central.insurance i\n" +
            "   <where>" +
            "       1 = 1" +
            "       <if test='name != null and !name.isEmpty()'>" +
            "           AND i.name ILIKE '%'||#{name}||'%'" +
            "       </if>" +
            "       <if test='region != null and !region.isEmpty()'>" +
            "           AND i.region ILIKE #{region}||'%'" +
            "       </if>" +
            "       <if test='city != null and !city.isEmpty()'>" +
            "           AND i.city ILIKE #{city}||'%'" +
            "       </if>" +
            "       <if test='address != null and !address.isEmpty()'>" +
            "           AND i.address ILIKE '%'||#{address}||'%'" +
            "       </if>" +
            "      <if test='inn != null'>" +
            "           AND i.inn = #{inn}" +
            "       </if>" +
            "       <if test='ogrn != null'>" +
            "           AND i.ogrn = #{ogrn}" +
            "       </if>" +
            "   </where>" +
            "   ORDER BY i.name" +
            "</script>")
    Collection<Insurance> getByFilter(Insurance insurance);
}
