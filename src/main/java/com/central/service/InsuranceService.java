package com.central.service;

import com.central.model.Insurance;

import java.util.Collection;

public interface InsuranceService {

    void save(Insurance insurance);

    Collection<Insurance> getByFilter(Insurance insurance);
}
