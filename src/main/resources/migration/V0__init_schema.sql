CREATE SCHEMA central;

DO $$
BEGIN

  IF NOT EXISTS(SELECT 1
                FROM pg_tables
                WHERE schemaname = 'central' AND tablename = 'insurance')
  THEN

    CREATE TABLE central.insurance (
      id      SERIAL      NOT NULL CONSTRAINT insurance_pkey PRIMARY KEY,
      name    TEXT        NOT NULL,
      region  TEXT        NOT NULL,
      city    TEXT        NOT NULL,
      address TEXT        NOT NULL,
      house   INTEGER     NOT NULL,
      inn     NUMERIC(10) NOT NULL,
      ogrn    NUMERIC(13) NOT NULL
    );

  END IF;

END $$;
